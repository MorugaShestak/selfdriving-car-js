// Создаем класс Controls
class Controls{
 // Конструктор принимает тип управления
 constructor(type){
     // Устанавливаем начальные значения для направлений движения
     this.forward=false;
     this.left=false;
     this.right=false;
     this.reverse=false;

     // В зависимости от типа управления добавляем обработчики событий
     switch(type) {
         // Если тип управления - клавиатура
         case "KEYS":
             this.#addKeyboardListeners();
             break;
         // Если тип управления - DUMMY
         case "DUMMY":
             // Устанавливаем значение reverse в true
             this.reverse = true;
             break;
     }
 }

 // Приватный метод для добавления обработчиков событий клавиатуры
 #addKeyboardListeners(){
     // Добавляем обработчик нажатия клавиш
     document.onkeydown=(event)=>{
         // В зависимости от нажатой клавиши устанавливаем соответствующее направление движения
         switch(event.key){
             case "ArrowLeft":
                 this.left=true;
                 break;
             case "ArrowRight":
                 this.right=true;
                 break;
             case "ArrowUp":
                 this.forward=true;
                 break;
             case "ArrowDown":
                 this.reverse=true;
                 break;
         }
     }
     // Добавляем обработчик отпускания клавиш
     document.onkeyup=(event)=>{
         // В зависимости от отпущенной клавиши сбрасываем соответствующее направление движения
         switch(event.key){
             case "ArrowLeft":
                 this.left=false;
                 break;
             case "ArrowRight":
                 this.right=false;
                 break;
             case "ArrowUp":
                 this.forward=false;
                 break;
             case "ArrowDown":
                 this.reverse=false;
                 break;
         }
     }
 }
}
