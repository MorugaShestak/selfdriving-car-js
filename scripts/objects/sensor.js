// Класс Sensor отвечает за создание и обновление лучей, которые будут использоваться для определения расстояния до препятствий на дороге
class Sensor {
 constructor(car) {
     this.car = car; // машина, для которой создаются лучи
     this.rayCount = 10; // количество лучей
     this.rayLength = 500; // длина лучей
     this.raySpread = Math.PI/2; // угол между первым и последним лучом

     this.rays = []; // массив лучей
     this.readings = []; // массив расстояний до препятствий
 }

 // метод обновления лучей
 update(roadBorders, traffic){
     this.#castRays(); // создание лучей
     this.readings=[]; // очистка массива расстояний
     for(let i=0;i<this.rays.length;i++){
         this.readings.push(
             this.#getReading(this.rays[i],roadBorders, traffic) // определение расстояния до препятствия для каждого луча
         );
     }
 }

 // метод создания лучей
 #castRays() {
     this.rays = []; // очистка массива лучей
     for (let i = 0; i < this.rayCount; i++) {
         const rayAngle = lerp(
             this.raySpread / 2,
             -this.raySpread / 2,
             this.rayCount === 1 ? 0.5 : i/(this.rayCount-1)
         )+this.car.angle; // угол поворота луча

         const start = {x:this.car.x, y:this.car.y}; // начальная точка луча
         const end = {
             x:this.car.x -
                 Math.sin(rayAngle) * this.rayLength, // конечная точка луча по оси x
             y:this.car.y -
                 Math.cos(rayAngle) * this.rayLength // конечная точка луча по оси y
         };
         this.rays.push([start,end]); // добавление луча в массив лучей
     }
 }

 // метод отрисовки лучей
 draw(ctx) {
     for(let i = 0; i < this.rayCount; i++) {
         let end = this.rays[i][1];
         if(this.readings[i]) {
             end = this.readings[i];
         }

         ctx.beginPath();
         ctx.lineWidth = 2;
         ctx.strokeStyle = "yellow";
         ctx.moveTo(this.rays[i][0].x, this.rays[i][0].y);
         ctx.lineTo(end.x, end.y);
         ctx.stroke();

         ctx.beginPath();
         ctx.lineWidth = 2;
         ctx.strokeStyle = "red";
         ctx.moveTo(this.rays[i][1].x, this.rays[i][1].y);
         ctx.lineTo(end.x, end.y);
         ctx.stroke();
     }
 }

 // метод определения расстояния до препятствия
 #getReading(ray, roadBorders, traffic) {
     let touches = [];

     for(let i = 0; i<roadBorders.length; i++) {
         const touch = getIntersection(
             ray[0],
             ray[1],
             roadBorders[i][0],
             roadBorders[i][1]
         );
         if(touch) {
             touches.push(touch)
         }

         for(let i = 0; i <traffic.length; i++) {
             const poly = traffic[i].polygon;
             for(let j = 0; j < poly.length; j++) {
                 const value = getIntersection(ray[0], ray[1], poly[j], poly[(j+1)%poly.length]);
                 if(value) {
                     touches.push(value)
                 }
             }
         }
     }

     if(touches.length <= 0) {
         return null;
     } else {
         const offset = touches.map(e=>e.offset);
         const minOffset = Math.min(...offset);
         return touches.find(e=>e.offset === minOffset)
     }
 }
}
