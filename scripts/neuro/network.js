// Класс нейронной сети
class NeuralNetwork {
 // Конструктор принимает массив количества нейронов в каждом слое
 constructor (neuronCounts) {
   // Создаем массив уровней нейронов
   this.levels = [];
   // Проходимся по всем слоям, кроме последнего
   for (let i = 0; i < neuronCounts.length - 1; i++) {
     // Добавляем новый уровень в массив уровней
     this.levels.push(new Level(neuronCounts[i], neuronCounts[i + 1]));
   }
 }

 // Статический метод для прямого распространения сигнала по сети
 static feedForward(givenInputs, network) {
   // Получаем выходные значения первого уровня
   let outputs = Level.feedForward(givenInputs, network.levels[0])

   // Проходимся по всем остальным уровням
   for (let i = 1; i < network.levels.length; i++) {
     // Получаем выходные значения текущего уровня
     outputs = Level.feedForward(outputs, network.levels[i])
   }
   // Возвращаем выходные значения последнего уровня
   return outputs
 }

 // Статический метод для мутации сети
 static mutate(network, amount = 1) {
   // Проходимся по всем уровням сети
   network.levels.forEach(level => {

     // Мутируем смещения
     for (let i = 0; i < level.biases.length; i++) {
       level.biases[i] = lerp(level.biases[i], Math.random() * 2 - 1, amount);
     }

     // Мутируем веса
     for (let i = 0; i < level.weights.length; i++) {
       for (let j = 0; j < level.weights[i].length; j++) {
         level.weights[i][j] = lerp(level.weights[i][j], Math.random()*2-1, amount);
       }
     }

   });
 }
}
