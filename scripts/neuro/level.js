// Определяем класс Level
class Level {
    // Конструктор класса принимает количество входов и выходов
    constructor(inputCount, outputCount) {
        // Создаем массивы для входов, выходов и смещений
        this.inputs = new Array(inputCount);
        this.outputs = new Array(outputCount);
        this.biases = new Array(outputCount);

        // Создаем двумерный массив для весов
        this.weights = [];
        for (let i = 0; i < inputCount; i++) {
            this.weights[i] = new Array(outputCount);
        }

        // Вызываем метод #randomize для случайной инициализации весов и смещений
        Level.#randomize(this);
    }

    // Статический метод #randomize для случайной инициализации весов и смещений
    static #randomize(level) {
        // Заполняем массив весов случайными значениями от -1 до 1
        for (let i = 0; i < level.inputs.length; i++) {
            for (let j = 0; j < level.outputs.length; j++) {
                level.weights[i][j] = Math.random() * 2 - 1;
            }
        }

        // Заполняем массив смещений случайными значениями от -1 до 1
        for ( let i = 0; i < level.biases.length; i++ ) {
            level.biases[i] = Math.random() * 2 - 1;
        }
    }

    // Статический метод feedForward для прямого распространения сигнала
    static feedForward(givenInputs, level){

        // Заполняем массив входов значениями из параметра givenInputs
        for (let i = 0; i < level.inputs.length; i++) {
            level.inputs[i] = givenInputs[i];
        }

        // Вычисляем выходы на основе входов, весов и смещений
        for(let i = 0; i < level.outputs.length; i++)
        {

            let sum = 0;

            for(let j = 0; j < level.inputs.length; j++)
            {
                sum = sum + level.inputs[j]*level.weights[j][i];
            }

            // Если сумма больше смещения, то выход равен 1, иначе 0
            if (sum > level.biases[i]) {
                level.outputs[i] = 1;
            } else {
                level.outputs[i] = 0;
            }

        }

        // Возвращаем массив выходов
        return level.outputs;
    }
}
